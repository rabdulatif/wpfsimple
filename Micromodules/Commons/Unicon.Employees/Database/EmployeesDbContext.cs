﻿using Microsoft.EntityFrameworkCore;
using SB.Migrator.EntityFramework;
using Unicon.Cities.Database.Tables;

namespace Unicon.Cities
{
    /// <summary>
    /// 
    /// </summary>
    [SBMigration]
    public class EmployeesDbContext : CitiesDbContext
    {
        /// <summary>
        /// 
        /// </summary>
        public DbSet<Employee> Employees { get; set; }
    }
}