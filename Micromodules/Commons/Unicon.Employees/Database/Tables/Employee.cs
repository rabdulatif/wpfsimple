﻿using SB.Common.Logics.Business;
using System.ComponentModel.DataAnnotations.Schema;

namespace Unicon.Cities.Database.Tables
{
    /// <summary>
    /// 
    /// </summary>
    [Table("Employees")]
    public class Employee : IIdentified
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long CountryId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Country Country { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long CityId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public City City { get; set; }
    }
}
