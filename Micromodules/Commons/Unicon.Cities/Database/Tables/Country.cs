﻿using SB.Common.Logics.Business;
using System.ComponentModel.DataAnnotations.Schema;

namespace Unicon.Cities.Database.Tables
{
    /// <summary>
    /// 
    /// </summary>
    [Table("Countries")]
    public class Country : IIdentified
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
    }
}
