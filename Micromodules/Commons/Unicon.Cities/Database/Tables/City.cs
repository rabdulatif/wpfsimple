﻿using SB.Common.Logics.Business;
using System.ComponentModel.DataAnnotations.Schema;

namespace Unicon.Cities.Database.Tables
{
    /// <summary>
    /// 
    /// </summary>
    [Table("Cities")]
    public class City : IIdentified
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long CountryId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual Country Country { get; set; }
    }
}
