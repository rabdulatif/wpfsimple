﻿using Microsoft.EntityFrameworkCore;
using Root.Logics.Database;
using SB.Migrator.EntityFramework;
using Unicon.Cities.Database.Tables;

namespace Unicon.Cities
{
    /// <summary>
    /// 
    /// </summary>
    [SBMigration]
    public class CitiesDbContext : RootDbContext
    {
        /// <summary>
        /// 
        /// </summary>
        public DbSet<Country> Countries { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<City> Cities { get; set; }
    }
}