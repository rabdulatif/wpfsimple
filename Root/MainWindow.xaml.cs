﻿using SB.Common.Extensions;
using System.Windows;

namespace Root
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Title = "Test".ToSafeString();
        }
    }
}
