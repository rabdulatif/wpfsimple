﻿using SB.Migrator;
using SB.Migrator.EntityFramework;
using SB.Migrator.SqlServer.Logics.Database.Extensions;

namespace Root.Logics.Migrations
{
    /// <summary>
    /// 
    /// </summary>
    public class DatabaseMigration
    {
        /// <summary>
        /// 
        /// </summary>
        public const string ConnectionString = "Server=.\\SQLEXPRESS;Database=TestEFSql;Trusted_Connection=True;";

        /// <summary>
        /// 
        /// </summary>
        public static void Migrate()
        {
            MigrateManager.Create(ConnectionString)
                .UseSqlServerDatabase()
                .UseEfCodeTablesManager()
                .Migrate();
        }
    }
}
