﻿using System;

namespace Root.Logics.Application
{
    /// <summary>
    /// 
    /// </summary>
    public class UniconApplication
    {
        /// <summary>
        /// 
        /// </summary>
        public static UniconApplication Current { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public static bool IsInitialized { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        private UniconApplication()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public static void Initialize()
        {
            if (IsInitialized)
                throw new Exception("Unicon application already initialized");

            Current = new UniconApplication();
            IsInitialized = true;
        }
    }
}
