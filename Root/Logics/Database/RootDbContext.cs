﻿using Microsoft.EntityFrameworkCore;
using Root.Logics.Migrations;

namespace Root.Logics.Database
{
    /// <summary>
    /// 
    /// </summary>
    public class RootDbContext : DbContext
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(DatabaseMigration.ConnectionString);
        }
    }
}
