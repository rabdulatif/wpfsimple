﻿using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Unicon
{
    /// <summary>
    /// 
    /// </summary>
    public partial class App
    {/// <summary>
     /// 
     /// </summary>
        public const string LibrariesPath = "Libraries";

        /// <summary>
        /// Application Entry Point.
        /// </summary>
        [STAThreadAttribute()]
        [DebuggerNonUserCodeAttribute()]
        [GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public static void Main()
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomainOnAssemblyResolveLibraries;
            InitializeApplication();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static Assembly CurrentDomainOnAssemblyResolveLibraries(object sender, ResolveEventArgs args)
        {
            var name = new AssemblyName(args.Name).Name;
            var file = Path.Combine(LibrariesPath, name + ".dll");
            var fileInfo = new FileInfo(file);
            if (!fileInfo.Exists)
                return null;

            return Assembly.LoadFrom(fileInfo.FullName);
        }

        /// <summary>
        /// 
        /// </summary>
        public static void InitializeApplication()
        {
            App app = new App();
            app.InitializeComponent();
            app.Run();
        }
    }
}
