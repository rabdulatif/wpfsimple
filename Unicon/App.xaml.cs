﻿using Root.Logics.Database;
using Root.Logics.Migrations;
using System;
using System.Collections.Generic;
using System.Windows;
using Unicon.Cities;

namespace Unicon
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var list = new List<Type>();
            list.Add(typeof(RootDbContext));
            list.Add(typeof(CitiesDbContext));
            list.Add(typeof(EmployeesDbContext));

            DatabaseMigration.Migrate();
        }

    }
}
